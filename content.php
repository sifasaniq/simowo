<?php
	$pg=$_GET['pg'];
	switch($pg){
		default:
			require_once("home.php");
			break;
	
		// halaman human resource untuk human resource
		case "datawo";
			require_once("datawo.php");
			break;
		// halaman import data dari excel ke database
		case "import";
			require_once("import.php");
			break;
		// halaman human resource untuk logistik
		case "employeesl";
			require_once("employees.php");
			break;
		// halaman report WO
		case "report";
			require_once("report.php");
			break;
		// halaman status
		case "status";
			require_once("status.php");
			break;
		// halaman inbox
		case "inbox";
			require_once("inbox.php");
			break;
		// halaman outbox
		case "outbox";
			require_once("outbox.php");
			break;
		// halaman setting
		case "about";
			require_once("about.php");
			break;
		// halaman create employee
		case "createemp";
			require_once("createemp.php");
			break;
		// halaman create produk
		case "createproduk";
			require_once("createproduk.php");
			break;
		// halaman create customer
		case "createctr";
			require_once("createctr.php");
			break;
		// halaman create message
		case "createmsg";
			require_once("createmsg.php");
			break;
		// halaman reply message
		case "replymsg";
			require_once("replymsg.php");
			break;
		// halaman create user
		case "createusr";
			require_once("createusr.php");
			break;
		// halaman create mesin
		case "createmesin";
			require_once("createmesin.php");
			break;
		// halaman create sales
		case "updatests";
			require_once("updatests.php");
			break;
		case "search";
			require_once("search.php");
			break;
		case "dataS";
			require_once("data_search.php");
			break;
		case "user";
			require_once("user.php");
			break;
		case "deletests";
			require_once("delstatus.php");
			break;
		case "deleteuser";
			require_once("deluser.php");
			break;
		case "deletedata";
			require_once("deldata.php");
			break;
		case "user1";
			require_once("user1.php");
			break;
		case "user2";
			require_once("user2.php");
			break;
		case "user3";
			require_once("user3.php");
			break;
			
	}
?>
