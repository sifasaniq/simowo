<hr class="noscreen" />
		<!-- Content (Right Column) -->
		<div id="content" class="box">
			<h1>Import Data dari Excel</h1>
		<br> Tabel yang harus ada dalam excel
		<table>
		<tr>
		<td><h3>wilayah
		<td><h3>kandatel
		<td><h3>ndem
		<td><h3>nd
		<td><h3>nd_speedy
		<td><h3>citem
		<td><h3>mdf
		<td><h3>deskripsi
		<td><h3>tgl_reg
		<td><h3>nama
		<td><h3>kcontact<td><h3>jalan<td><h3>no_jalan<td><h3>distrik<td><h3>kota<td><h3>status<td><h3>tgl_update<td><h3>keterangan
		</table>
		<br/>
		<br>

<?php
//$con=mysqli_connect('localhost', 'root', '');
//mysqli_select_db($con, 'telkom');
mysqli_connect("localhost","root","","telkom");
//memanggil file excel_reader
require "excel_reader.php";

//jika tombol import ditekan
if(isset($_POST['submit'])){
?>
<div id="progress" style="width:500px;border:1px solid #ccc;"></div>
<div id="info"></div>
<?php
    $target = basename($_FILES['filepegawaiall']['name']) ;
    move_uploaded_file($_FILES['filepegawaiall']['tmp_name'], $target);
    
    $data = new Spreadsheet_Excel_Reader($_FILES['filepegawaiall']['name'],false);
    
//    menghitung jumlah baris file xls
    $baris = $data->rowcount($sheet_index=0);
    
//    jika kosongkan data dicentang jalankan kode berikut
    if(isset($_POST['drop'])==1){
//             kosongkan tabel data
             $truncate ="TRUNCATE TABLE tbl_data";
             mysqli_query($con,$truncate);
    };
    
//    import data excel mulai baris ke-2 (karena tabel xls ada header pada baris 1)
    for ($i=2; $i<=$baris; $i++)
    {
//   	  menghitung jumlah real data. Karena kita mulai pada baris ke-2, maka jumlah baris yang sebenarnya adalah 
//        jumlah baris data dikurangi 1. Demikian juga untuk awal dari pengulangan yaitu i juga dikurangi 1
        $barisreal = $baris-1;
        $k = $i-1;
        
// menghitung persentase progress
        $percent = intval($k/$barisreal * 100)."%";
 
// mengupdate progress
        echo '<script language="javascript">
        document.getElementById("progress").innerHTML="<div style=\"width:'.$percent.'; background-color:lightblue\">&nbsp;</div>";
        document.getElementById("info").innerHTML="'.$k.' data berhasil diinsert ('.$percent.' selesai).";
        </script>';
//       membaca data (kolom ke-1 sd terakhir)
      $wilayah          = $data->val($i, 1);
      $kandatel   	  	= $data->val($i, 2);
	  $ndem				= $data->val($i, 3);
	  $nd				= $data->val ($i, 5);
	  $nd_speedy		= $data->val ($i, 6);
	  $citem			= $data->val ($i, 7);
	  $mdf				= $data->val ($i, 8);
	  $deskripsi		= $data->val ($i, 9);
	  $tgl_reg			= $data->val ($i, 10);
	  $nama				= $data->val ($i, 13);
	  $kcontact			= $data->val ($i, 14);
	  $jalan			= $data->val ($i, 15);
	  $no_jalan			= $data->val ($i, 16);
	  $distrik			= $data->val ($i, 17);
	  $kota				= $data->val ($i, 18);
	  $status			= $data->val ($i, 26);
	  $tgl_update		= $data->val ($i, 27);
	  $keterangan		= $data->val ($i, 28);
	  $nol = "0";

//      setelah data dibaca, masukkan ke tabel pegawai sql
      $query = "INSERT into tbl_data (wilayah, kandatel, ndem, nd, nd_speedy, citem, 
									  mdf, deskripsi, tgl_reg, nama, kcontact, jalan, no_jalan,
									  distrik, kota, status, tgl_update, keterangan)
							   values('$wilayah', '$kandatel', '$ndem', '$nol$nd', '$nd_speedy', '$citem', 
									  '$mdf', '$deskripsi', '$tgl_reg', '$nama', '$kcontact', '$jalan', '$no_jalan',
									  '$distrik', '$kota','$status', '$tgl_update', '$keterangan')";
      $hasil = mysqli_query($con,$query);
	  flush();
    }
    
    if(!$hasil){
//          jika import gagal
          die(mysqli_error());
      }else{
//          jika impor berhasil			
			echo "Data berhasil disimpan dalam database!";
			echo "<br>";
			
          
		  
    }
    
//    hapus file xls yang udah dibaca
   // unlink($_FILES['filepegawaiall']['name']);
}

?>
<form name="myForm" id="myForm" onSubmit="return validateForm()" header="location:index.php?pg=pg_import" method="post" enctype="multipart/form-data">
    <input type="file" id="filepegawaiall" name="filepegawaiall" />
	<br><br>
	<label><input type="checkbox" name="drop" value="1" /> <u>Kosongkan tabel sql terlebih dahulu.</u> </label>
	<br>
    <input class="btn btn-danger btn-xs" type="submit" name="submit" value="Import" /><br/>
	<br>
    
</form>

<script type="text/javascript">
//    validasi form (hanya file .xls yang diijinkan)
    function validateForm()
    {
        function hasExtension(inputID, exts) {
            var fileName = document.getElementById(inputID).value;
            return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
        }

        if(!hasExtension('filepegawaiall', ['.xls'])){
            alert("Hanya file XLS (Excel 2003) yang diijinkan.");
            return false;
        }
    }
</script>

</div> <!-- /content -->

	</div> <!-- /cols -->
	
	<hr class="noscreen" />
