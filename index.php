<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="content-language" content="en" />
	<meta name="robots" content="noindex,nofollow" />
	<link rel="stylesheet" href="css/futurico-theme.css" media="screen">
	<link rel="stylesheet" href="css/nivo-slider.css" media="screen">
    <link id="bs-css" href="css/bootstrap-cerulean.css" rel="stylesheet">
	<link rel="stylesheet" media="screen,projection" type="text/css" href="css/reset.css" /> <!-- RESET -->
	<link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css" /> <!-- MAIN STYLE SHEET -->
	<link rel="stylesheet" media="screen,projection" type="text/css" href="css/2col.css" title="2col" /> <!-- DEFAULT: 2 COLUMNS -->
	<link rel="alternate stylesheet" media="screen,projection" type="text/css" href="css/1col.css" title="1col" /> <!-- ALTERNATE: 1 COLUMN -->
	<!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]--> <!-- MSIE6 -->
	<link rel="stylesheet" media="screen,projection" type="text/css" href="css/style.css" /> <!-- GRAPHIC THEME -->
	<!--<link rel="stylesheet" media="screen,projection" type="text/css" href="css/mystyle.css" /> <!-- WRITE YOUR CSS CODE HERE -->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/switcher.js"></script>
	<script type="text/javascript" src="js/toggle.js"></script>
	<script type="text/javascript" src="js/ui.core.js"></script>
	<script type="text/javascript" src="js/ui.tabs.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".tabs > ul").tabs();
	});
	</script>
	<title>Indihome</title>
</head>

<body >

<div id="main" >

	<!-- Tray -->
	<div id="tray" class="box">

		<p class="f-left box">

			<!-- Switcher -->
			<span class="f-left" id="switcher">
				<a href="#" rel="1col" class="styleswitch ico-col1" title="Display one column"><img src="design/switcher-1col.gif" alt="1 Column" /></a>
				<a href="#" rel="2col" class="styleswitch ico-col2" title="Display two columns"><img src="design/switcher-2col.gif" alt="2 Columns" /></a>
			</span>

			Telkom Indonesia: <strong>IndiHome</strong>

		</p>
		<?php
			include 'connect.php';
			if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true){
		?>
        	<p class="f-right">User: <strong><a href="#"><?php echo $_SESSION['username']; ?></a></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><a href="logout.php" id="logout">Log out</a></strong></p>
        <?php
			}
			else{
				header('location:login.php');
			}
		?>
				
	</div> <!--  /tray -->

	<hr class="noscreen" />

	<!-- Menu -->
	<div id="menu" class="box">

		<ul class="box">
        	<?php
				if(isset($_GET['pg'])){
					$pg=$_GET['pg'];
				}
			?><?php
				if($_SESSION['userlevel']!='admin' && $_SESSION['userlevel']!='karyawan'){
			?>
            <li <?php if(isset($pg) && ($pg=='import' || $pg=='status')) echo 'id="menu-active"'; ?>><a href="?pg=import"><span>Import Data</span></a></li>
			<?php
				}
				if($_SESSION['userlevel']=='karyawan'||$_SESSION['userlevel']=='admin'){
			?>
            <li <?php if(isset($pg) && ($pg=='datawo'|| $pg=='createemp' || $pg=='reademphr' || $pg=='createsls'|| $pg=='updatests')) echo 'id="menu-active"'; ?>><a href="?pg=datawo"><span>Data</span></a></li>
			<?php
				}
				if($_SESSION['userlevel']=='karyawan'||$_SESSION['userlevel']=='admin' ){				
			?>
			<li <?php if(isset($pg) && ($pg=='inbox' || $pg=='outbox' || $pg=='createmsg'|| $pg=='replymsg')) echo 'id="menu-active"'; ?>><a href="?pg=inbox"><span>Mesagging</span></a></li>
			<?php
				}
				if($_SESSION['userlevel']=='admin' ){
			?>
			<li <?php if(isset($pg) && ($pg=='user'||$pg=='createusr')) echo 'id="menu-active"'; ?>><a href="?pg=user"><span>Manage User</span></a></li>
			<?php	
				}
				if($_SESSION['userlevel']=='karyawan'||$_SESSION['userlevel']=='admin' ){
			?>
			<li <?php if(isset($pg) && ($pg=='report')) echo 'id="menu-active"'; ?>><a href="?pg=report"><span>Report</span></a></li>
			<?php	
				}
				if($_SESSION['userlevel']=='karyawan'||$_SESSION['userlevel']=='admin' )
			?>
		</ul>

	</div> <!-- /header -->

	<hr class="noscreen" />

	<!-- Columns -->
	<div id="cols" class="box">

		<!-- Aside (Left Column) -->
		<div id="aside" class="box">

			<div class="padding box">

				<!-- Logo (Max. width = 200px) -->
				<p id="logo"><a href="index.php"><img src="tmp/logo.jpg" alt="Our logo" title="Visit Site" /></a></p>
			</div> <!-- /padding -->
			<?php
				if(isset($pg) && ($pg=='datawo' || $pg=='updatests' )){
			?>
			<ul class="box">
				<li>Data</li>
					<ul>
						<li><a href="?pg=datawo">Data Customer</a></li>
					</ul>
			</ul>
			<?php
				}
                    if(isset($pg) && ($pg=='report')){
                        ?>
                    <ul class="box">
                    <li>Data Report</li>
                    <ul>
                    <li><a href="?pg=report">Report</a></li>
                    </ul>
                    </ul>
                    <?php
                        }
				if(isset($pg) && ($pg=='inbox' || $pg=='outbox' || $pg=='createmsg'|| $pg=='replymsg')){
			?>
			<ul class="box">
				<li>Messaging</li>
					<ul>
						<li><a href="?pg=createmsg">Compose New Message</a></li>
						<li><a href="?pg=inbox">Inbox</a></li>
						<li><a href="?pg=outbox">Outbox</a></li>
					</ul>
			</ul>
			<?php
				}
				if(isset($pg) && ($pg=='about'||$pg=='user1'||$pg=='user2'||$pg=='user3')){
			?>
			<?php
				}
				if(isset($pg) && ($pg=='import' || $pg=='status' || $pg=='readmesin' || $pg=='createproduk' || $pg=='readproduk')){
			?>
			<ul class="box">
				<li>Import Data</li>
					<ul>
						<li><a href="?pg=import">Import dari Excel</a></li>
					</ul>
				<li>Tambah Status</li>
					<ul>
						<li><a href="?pg=status">Tambah Status</a></li>
					</ul>
			</ul>
			<?php
				}
				
				if(isset($pg) && ($pg=='createusr' || $pg=='user' )){
			?>
			<ul class="box">
				<li>Data User</li>
					<ul>
						<li><a href="?pg=user">Data User</a></li>
					</ul>				
			</ul>
			<?php
				}
			
			?>
		</div> <!-- /aside -->

		<?php 
		
        if(isset($_GET['pg'])) include "content.php"; 
			else include "home.php";
		?>
	<!-- Footer -->
	<div id="footer" class="box">

		<p class="f-left">&copy; 2015 <a href="#">Telkom Indonesia</a>, All Rights Reserved &reg;</p>

		<p class="f-right">Templates by <a href="http://www.adminizio.com/">Adminizio</a></p>

	</div> <!-- /footer -->

</div> <!-- /main -->

</body>
</html>
