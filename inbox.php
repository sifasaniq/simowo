	<hr class="noscreen" />

		<!-- Content (Right Column) -->
		<div id="content" class="box">

			<h1>Inbox</h1>
			</br>
			<a href="?pg=createmsg" class="btn btn-danger btn-xs">Compose a new message</a>
			</br></br>
			<table class="table table-condensed">
				<tr>
					<th>No.</th>
					<th>Sender</th>
					<th>Message</th>
					<th>Date</th>
					<th>Action</th>
				</tr>
			<?php 
				$query=mysqli_query($con,"SELECT * from tbl_pesan");
				$i=1;
				while($result=mysqli_fetch_array($query)){
				if($result['penerima']==$_SESSION['username']){
			?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $result['pengirim']; ?></td>
					<td><?php echo $result['pesan']; ?></td>
					<td><?php echo $result['tanggal']; ?></td>
					<td><center><a href="?pg=replymsg&to=<?php echo $result['pengirim']; ?>" class="btn btn-danger btn-xs">Reply</a></center></td>
					
				</tr>
			<?php
					$i++;
				}
				}
			?>
			</table>
		</div> <!-- /content -->

	</div> <!-- /cols -->

	<hr class="noscreen" />